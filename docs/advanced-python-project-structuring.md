# 고급 Python 프로젝트 구조화

## Python 패키지 Authority, 일명 PyPA
Python은 범용 프로그래밍 언어로, 웹 어플리케이션, 데스크톱 어플리케이션, 임베디드 시스템등 다양한 분야의 소프트웨어를 개발하는 데 사용할 수 있다.

PyPA에는 여러분이 매일 사용했거나 사용 중인 도구로 구성된 방대한 생태계를 지원한다.

- [**pip**](https://github.com/pypa/pip) - Python 패키지 설치 프로그램으로 패키지를 설치, 제거, 업데이트 등과 같이 패키지를 관리하는 데 사용한다. 새 패키지를 설치할 때 pip은 PyPI 리포지토리에서 패키지를 다운로드한다.
- [**virtualenv**](https://github.com/pypa/virtualenv) - 작업하는 각 프로젝트에 대해 격리된 Python 환경을 만드는 데 사용되는 도구이다. 더 자세히 알고 싶으시다면 [What The Heck Is A Virtual Environment — Python](https://python.plainenglish.io/what-the-heck-is-virtual-environment-7492d2338f4a)을 참고하세요.
- [**twine**](https://github.com/pypa/twine) - 패키지를 빌드한 후 PyPI에 업로드할 때 사용한다.

[PyPA](https://www.pypa.io/en/latest/)는 [PyPI](https://pypi.org/)와 같은 온라인 리포지토리에 Python 코드를 빌드, 패키징 및 배포하는 데 사용되는 핵심 소프트웨어 세트를 관리하는 커뮤니티이다.

PyPI에 익숙하지 않은 사람들을 위해 설명하자면, 자신의 Python 프로젝트를 게시할 수 있고 다른 사람의 프로젝트를 다운로드하여 설치할 수 있는 저장소이다.

`pip`을 사용하여 패키지를 설치했다면 PyPI를 특정 방식으로 사용한 것이다. `pip`은 프로젝트 종속성(이 경우 프로젝트 종속성)을 관리하기 위해 PyPA에서 제공하는 도구 중 하나이기도 하다(나중에 자세히 설명하겠다).

프로젝트를 패키징할 때 PyPA에서 권장하는 몇 가지 모범 사례를 따라하면 작업을 쉽게 할 수 있다.

- 대상 사용자 - 누가 우리 시스템을 사용할 것인가? 내부(회사 내)에서 사용할 것인가, 그렇다면 다른 개발자나 다른 부서에서 사용할 것인가?
- 시스템이 실행되는 위치 - 주기적으로 실행되는 스크립트인가, 데스크톱 어플리케이션인가, 웹 어플리케이션인가 등?
- "소프트웨어가 개별적으로 설치되나요, 아니면 대규모 배포 배치로 설치되나?"

프로젝트의 목표에 따라 프로젝트에 적합한 패키징 솔루션을 정의해야 한다.

예를 들어, 프로젝트가 API나 웹 어플리케이션이라면 라이브러리나 임베디드 시스템과는 접근 방식이 다를 수 있다.

PyPI 퍼블릭 리포지토리에 게시하여 라이브러리를 배포하는 것이 목표인 경우 PyPA에서 권장하는 `setuptools` 및 `wheel`과 같은 도구를 사용할 수 있다.

이 섹션에서 다룰 내용이 바로 그것이다. 모듈과 패키지와 같은 기본적인 프로젝트 구조 개념을 확실히 이해하고 있어야 한다.

라이브러리는 특정 작업을 수행하기 위해 공유할 수 있는 단일 모듈(Python 파일)일 수도 있고, 패키지를 구성하는 여러 모듈이 포함된 더 복잡한 프로젝트(모듈이 포함된 폴더)일 수도 있다.

프로젝트를 설정하고 게시할 준비가 되도록 빌드하는 방법을 살펴보겠다.

### 프로젝트 설정
PyPA에서 제공하는 많은 도구 중 하나인 `setuptools`를 사용하여 프로젝트를 쉽게 패키징할 수 있도록 정의할 수 있다.

`setuptools`를 사용하면 `pip`를 사용하여 설치할 수 있는 프로젝트를 "라이브러리 형태로" 공유할 수 있다. 라이브러리는 앞서 설명한 대로 PyPI 리포지토리에 업로드할 수 있다.

프로젝트를 빌드할 수 있도록 설정하려면 먼저 프로젝트를 구성해야 한다. 구성은 `pyproject.toml`, `setup.py` 또는 `setup.cfg` 파일을 사용하여 수행할 수 있다.

![](./images/1_Zb76ykyAbnxcp3GMblmuUA.webp)

하지만 기본 파일이자 설정 도구에서 권장하는 파일은 [`pyproject.toml`](https://pip.pypa.io/en/stable/reference/build-system/pyproject-toml/)이므로 몇 가지 기본적인 구성으로 파일을 만드는 방법에 대해 알아보겠다.

```
[build-system]
requires = ["setuptools"]
```

가장 먼저 지정해야 할 것은 사용할 `build-system` 테이블이다. 이 테이블은 [PEP 518](https://peps.python.org/pep-0518/)에 도입되었으며 여러 키를 가질 수 있지만 `requires` 키는 필수이다.

`requires` 키는 빌드 환경에서 `pip`을 사용하여 설치할 프로젝트 요구 사항의 리스트를 받는다.

[`pyproject.toml`](https://pip.pypa.io/en/stable/reference/build-system/pyproject-toml/) 파일에 `build-system` 테이블을 정의하지 않으면 빌드할 때 기본 테이블이 추가된다.

```
[build-system]
requires = ["setuptools>=40.8.0", "wheel"]
build-backend = "setuptools.build_meta:__legacy__"
```

`build-system` 테이블을 지정한 후에는 프로젝트를 패키징하는 데 필요한 나머지 정보가 포함된 다른 테이블을 추가해야 한다.

```
[project]
name = "your-project-name"
version = "0.0.1"
dependencies = [
    "fastapi",
    'python_version<"3.8"',
]
```

`project` 테이블은 개발 중에 사용되는 라이브러리의 이름, 버전, 종속성 등을 정의하는 곳이다.

`build-system` 테이블의 `requires`와 프로젝트 테이블의 `dependencies`의 차이점이 궁금하다면, `requires`는 패키지를 빌드하는 데 필요한 요구 사항을 받는다는 차이점이 있다.

반면 `dependencies`에서는 `requirements` 파일이나 `conda`를 사용하는 경우 `environment.yml` 파일에 있는 모든 프로젝트 종속성을 선언한다.

프로젝트 구조가 복잡하여 일부 파일과 폴더를 제외하고 최종 빌드 프로젝트에 추가되지 않도록 해야 하는 경우 특별한 주의가 필요하다.

이 모든 것을 지정하는 테이블은 `tool-setuptools-packages-find`이며, 테이블의 모든 키는 선택 사항이다.

```
[tool.setuptools.packages.find]
where = ["src"]
include = ["my-project*"] 
exclude = ["my-project.tests*"]
```

`pyproject`에 대한 더 많은 테이블과 키를 [여기](https://setuptools.pypa.io/en/latest/userguide/pyproject_config.html)에서 확인할 수 있다.

완벽하다! 이제 구성과 프로젝트 실행에 필요한 모든 종속성을 정의했으니 다음 단계인 라이브러리 빌드로 넘어가겠다.

프로젝트를 빌드하여 PyPI 저장소에 업로드하기 전에 먼저 인터프리터에서 빌드하여 로컬에서 테스트하고 `pip` 명령을 사용하여 설치한 것처럼 작업할 수 있도록 모방해야 한다.

이렇게 하려면 다음 명령을 실행하면 된다.

```bash
$ pip install --editable . # the dot (.) represents your current project directory
```

이제 자신감이 생기고 빌드할 준비가 되었으면 먼저 `build` 라이브러리를 설치해야 한다.

`build`는 생성한 `pyproject.toml` 파일을 사용하며, 제공된 정보를 기반으로 소스 배포([`sdist`](https://packaging.python.org/en/latest/glossary/#term-Source-Distribution-or-sdist))와 wheels(바이너리 배포)을 생성한다.

`build` 라이브러리를 설치하려면 다음 명령을 실행한다.

```bash
$ python3 -m pip install build
```

다음 명령을 실행하여`sdist`와 `wheels`을 사용하여 라이브러리를 빌드한다.

```bash
$ python3 -m build --sdist --wheel
```

이제 마지막 단계는 프로젝트를 PyPI에 업로드하거나 퍼불리쉬하는 것이다. 다행히도 이 단계는 이전 단계와 마찬가지로 매우 쉽고 간단하다.

[PyPI](https://pypi.org/)로 푸시하기 전에 [계정](https://pypi.org/account/register/)이 있는지, Twine이 설치되어 있는지 확인하자.

```bash
$ pip install twine
```

퍼블리쉬하려면 다음 명령을 실행하면 계정 자격 증명을 입력하라는 메시지가 표시된다.

```bash
$ twine upload dist/*
```

프로젝트를 업로드한 후 [PyPI](https://pypi.org/)의 https://pypi.org/project/<my-project>에서 찾을 수 있다.

## 프로젝트 종속성 관리
각 Python 프로젝트에는 개발을 위해 설치해야 하는 고유한 요구사항이 있을 것이다.

이러한 종속성을 적절하게 관리하지 않으면 같은 컴퓨터에서 실행 중인 다른 프로젝트와 충돌이 발생할 수 있다. 그렇기 때문에 첫 번째 단계는 프로젝트를 위한 가상 환경을 만드는 것이다.

PyPA를 사용하면 다음 명령을 실행하는 것만큼이나 쉽게 가상 환경을 만들 수 있다.

```bash
$ python -m pip install --user virtualenv # install the virtualenv
$ virtualenv <env_name> # create the virtual environment
```

가상 환경에 대한 자세한 내용은 [What The Heck Is A Virtual Environment — Python](https://yanick-andrade.medium.com/what-the-heck-is-virtual-environment-7492d2338f4a?source=post_page-----d74f342e58f0--------------------------------)에서 얻을 수 있다.

프로젝트는 이식성이 있어야 하기 때문에 개발 환경에서 테스트 환경이나 프로덕션 환경으로 이동할 때 문제 없이 실행되도록 하려면 모든 종속성(요구 사항)이 잘 문서화되어 있는지 확인해야 한다.

요구 사항을 처리하는 방법에는 두 가지가 있다. 하나는 요구 사항을 설치할 때마다 요구 사항 파일에 추가하는 것이며, 다른 하나는 가상 환경을 고정하는 방법이다.

첫 번째 방법이 좀 더 깔끔하지만 `pip install package`명령을 실행할 때마다 추가해야 한다는 점이다.

두 번째 방법이 더 간편하며 요구 사항 파일에 종속성을 추가하는 것을 잊은 경우에도 걱정할 필요가 없다.

```bash
$ pip freeze > requirements.txt # saves all packages installed in requirements file
```

프로젝트 종속성을 적절하게 관리한다면 프로젝트가 어떤 환경에서든 동일하게 실행되는 데 필요한 모든 것을 갖출 수 있다.

지금까지 이 섹션에서는 가상 환경을 사용하여 환경을 관리하는 방법에 대해 설명했지만 이것이 유일한 도구는 아니다.

[`Anaconda`](https://www.anaconda.com/)에서 환경을 관리하기 위해 제공하는 도구인 [`conda`](https://anaconda.org/anaconda/conda)도 있다. `conda`를 사용하여 환경을 만들려면 먼저 [Anaconda](https://www.anaconda.com/download/)가 설치되어 있는지 확인한 후 다음 명령을 실행한다.

```bash
$ conda create --name <env_name> # create a new conda environment
```

특정 Python 버전으로 `conda` 환경을 만들려면 다음 매개 변수를 추가하여 생성 명령을 약간 변경한다.

```bash
$ conda create -n <env_name> python=<version number> # 3.7, 3.8, 3.9, 3.11, 3.11
```

`conda`를 사용할 때 이전처럼 요구 사항 파일을 사용하여 종속성을 관리할 수도 있으며, 여전히 `pip`를 사용하여 패키지를 설치할 수 있지만 가능하면 conda를 사용하여 패키지를 설치하는 것이 좋다.

`conda` 환경을 활성화하려면 다음 명령을 실행한다.

```bash
$ conda activate <env_name>
```

`conda`에서 파일에 대한 종속성을 고정하려면 다음 명령을 사용한다.

```bash
$ conda env export > environment.yml
```

`conda`를 사용하면 다른 곳에서 내보낸 `environment.yml` 파일로 환경을 복원할 수 있다.

```bash
$ conda env create -f environment.yml
```

그렇다면 어떤 것을 선택해야 할까? 이 질문에 대한 답변으로 가상 환경을 사용하는 것이 바람직 하다. 더 쉽고 널리 사용된다.

아시다시피 Anaconda는 데이터 과학에서 사용하는 것이 적합하다.

## Pytest를 사용한 프로젝트 테스팅
많은 개발자들이 커리어를 쌓는 동안 가장 소홀히 하는 것 중 하나가 바로 테스팅이다. 저자가 근무하는 지금 회사에서는 코드 베이스의 80%를 테스트해야 한다는 요구 사항이 있기 때문에 비로서 테스트에 집중하기 시작했다고 한다.

모든 테스트 케이스를 작성하는 것은 고통스럽지만 매우 중요하다.

[단위 테스트](https://www.testim.io/blog/unit-test-vs-integration-test/), [통합 테스트](https://www.testim.io/blog/unit-test-vs-integration-test/) 또는 둘 다 수행하든 관계없이 [Pytest](https://docs.pytest.org/en/7.4.x/contents.html)를 사용하여 테스트를 작성할 수 있다.

여기서는 테스트에 대해 자세히 설명하지는 않겠지만, 이해를 돕기 위해 단위 테스트가 무엇이고 통합 테스트가 무엇인지 살펴보자.

*단위 테스트*는 코드의 각 부분(단위)을 개별적으로 테스트하여 의도한 대로 작동하는지 확인하는 방법론이다. 코드 베이스에서 각 함수  또는 메소드를 테스트하는 것이라 할 수 있다.

반면에 *통합 테스트*는 모든 단위를 결합하여 종속성과 흐름이 의도한 대로 작동하는지 확인하는 테스트이다.

![](./images/1_Tud_NdLexr41dKSnjeROVg.webp)

테스트 프레임워크로 [Pytest](https://docs.pytest.org/en/7.4.x/contents.html)를 선택한 이유는 소규모로 시작하여 프로젝트가 성장함에 따라 쉽게 확장할 수 있기 때문이다.

`Pytest`의 몇 가지 기능을 살펴보겠다.

- **출력 메시지**: Pytest는 실패한 테스트에 대한 자세한 메시지를 제공하므로 코드를 더 쉽게 수정할 수 있다.
- **fixture**: 가장 많이 사용되는 기능 중 하나는 Pytest의 fixture이다(곧 자세히 살펴보자).
- **플러그인**: 비동기 함수를 테스트하고 싶을 때 사용할 수 있는 플러그인이 있다. 모의 테스트가 필요하다면 이를 위한 플러그인도 있다. 800개 이상의 플러그인을 사용할 수 있다.
- **커뮤니티**: 참여적이고 주도적인 커뮤니티.
- **Python `unitetst` 지원**: Pytest는 Python의 기본 제공 단위 테스트 라이브러리를 즉시 지원한다.
- **문서**: 마지막으로, 원활한 시작을 도와주는 훌륭한 문서를 제공한다.

[`Pytest`](https://docs.pytest.org/en/7.4.x/getting-started.html#install-pytest)를 시작하려면 먼저 가상 환경에 설치되어 있는지 확인한다.

```bash
$ pip install -U pytest
```

간단한 테스트 시나리오는 다음과 같다.

```python
def print_hello() -> str:
    return "Hello"


def test_print_hello():
   # this will fail
    assert print_hello() == "hello"
```

테스트를 실행하려면 다음 명령을 실행하기만 하면 된다.

```bash
$ pytest
```

테스트 폴더와 하위 폴더 내의 모든 테스트를 실행한다. 테스트 파일과 테스트 함수의 이름 지정 규칙은 `test_<filename>`과 `test_<function_name>`이다.

때로는 테스트가 제대로 실행되기 위해 매개변수, 즉 종속성이 필요할 수 있다. 종속성은 데이터베이스 연결, 네트워크 연결 또는 기타 모든 것이 될 수 있다.

Pytest는 여러 단위 테스트에 쉽게 "주입"할 수 있는 테스트 종속성을 생성하는 방법인 `fixture`를 제공한다.

```python
import pytest


@pytest.fixture
def hello() -> str:
    return "hello"


# hello parameter is the fixture defined
def test_say_hello(hello):
    assert hello == "hello"

def test_say_hello_world(hello):
    assert f"{hello}, world" == "hello, world"
```

이 시나리오에서는 단일 테스트 파일에 포함된 fixture를 사용하고 있다. 하지만 서로 다른 테스트 파일에 동일한 정보, 즉 fixture가 필요하다면 어떻게 해야 할까?

테스트 폴더 내의 여러 테스트 파일에서 쉽게 액세스할 수 있는 공유 가능한 `fixture`를 만들려면 `conftest` 파일을 만들고 fixture를 추가해야 한다.

![](./images/1_OWoClJCDmGSOpZpbW9PNOg.webp)

`conftest.py` 파일은 다음과 같다.

```python
from typing import Any
import pytest


@pytest.fixture(scope="session")
def my_global_fixture() -> Any:
    pass
```

테스트 파일 전체에서 액세스할 수 있도록 하는 동작은 `@pytest.fixture(scope="session")`이다.

이렇게 하면 동일한 fixture를 반복적으로 생성하지 않고 테스트를 중단하지 않고 확장하는 데 도움이 된다.

## 자동화된 프로젝트 문서화
대부분의 개발자들은 프로젝트에 대해 문서를 작성하지 않기 때문에 아쉽게도 모든 프로젝트에 대하여도 그러하다. 

하지만 저자는 현재 회사와 이전 회사에서 일부 문서만을 작성했다. 새 프로젝트를 시작하고 몇 달이 지나면 제가 했던 일의 대부분을 잊어버릴 수 있다는 것을 알았기 때문에 문서를 작성하곤 하였다.

프로젝트를 문서화할 수 있는 방법은 Word 문서를 만들거나, Google 문서 도구로 작성하거나, [`Sphinx`](https://www.sphinx-doc.org/en/master/)와 같은 자동화된 도구를 사용하는 등 여러 가지가 있다.

`Sphinx`는 Python 생태계에서 PDF로 내보내거나 HTML 페이지를 생성하여 온라인 문서를 생성할 수 있는 문서를 생성하는 데 널리 사용되는 강력한 도구이다.

여러분이 보는 많은 라이브러리 문서가 `Sphinx`를 사용하여 작성되었기 때문에 많은 기능을 갖춘 멋진 온라인 문서를 쉽게 작성할 수 있다.

아래 클래스에 대한 간단한 문서를 만들어 보겠다.

```python
from typing import Any


class Car:
    """ 
    A simple class that represents a car in our system.
    """
    
    def __init__(self, brand: str, engine: str, year: int) -> None:
        """ Initialize the car class with the received arguments
        
        :param brand (str): the car's brand (i.e. BWM, Kia etc.)
        :param engine (str): it can be dieser or something else
        :param year (int): the manufacture year of the car
        """
        self.brand = brand
        self.engine = engine
        self.year = year


    def create(self, db) -> "Car":
        """Receives a DB session and a car object and save it in our database

        :param db: session connected to some database
        :return: Any
        """
        pass

    def update(self, car: "Car", db) -> Any:
        """Receives a DB session and update the car in our database

        :param car: car object
        :param db: session connected to some database
        :return: Any
        """
        pass
```

먼저 운영 체제에 따라 `Sphinx`가 설치되어 있는지 확인하거나 가상 환경설정에서 지침에 따라 다음 명령을 실행한다.

```bash
$ pip install -U sphinx
```

설치 후 터미널에서 프로젝트 디렉토리로 이동하여 다음 명령을 실행한다.

```bash
$ mkdir docs && cd docs && sphinx-quickstart
```

`mkdir` 명령을 사용하여 문서를 저장할 새 폴더(`docs/`)를 만들었다. 그런 다음 `sphinx-quickstart`를 사용하여 프로젝트의 `sphinx`를 초기화하기 전에 `cd docs` 명령을 실행하여 문서 폴더 안에 있는지 확인한다.

![](./images/1_VgtrCQsJwGNZrWIth24zsw.webp)

- `conf.py` - 프로젝트 구성 등과 같은 Sphnix에 대한 구성을 저장한다.
- `index.rst` - 이 파일은 기본 문서 파일로, 모듈 포함 등 문서의 구조를 제공한다.

먼저 프로젝트에 대한 문서를 만들려면 문서 폴더 내의 `conf.py`에서 몇 가지를 변경해야 하므로 다음과 같이 저장되어 있는지 확인한다.

```python
# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

import sys, os

sys.path.append(os.path.abspath('..'))

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information


project = 'Advanced Project Structuring'
copyright = '2023, Your Name'
author = 'Your Name'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = ['sphinx.ext.autodoc']

templates_path = ['_templates']
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']



# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'alabaster'
html_static_path = ['_static']
```

또한 프로젝트에서 여러 모듈에 대한 문서를 만들려면 `index.rst` 파일이 다음과 같이 저장되어 있는지 확인하세요.

```
.. Advanced Project Structuring documentation master file, created by
   sphinx-quickstart on Fri Dec 15 12:28:03 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Advanced Project Structuring's documentation!
========================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules # this is in case we have multiple modules in our code

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
```

이 작업을 완료했으면 이제 다음 명령을 실행하여 문서를 만들 수 있다.

```bash
$ sphinx-apidoc -o docs
```

생성된 모든 파일은 `docs` 폴더에 저장된다. 브라우저에서 문서를 시각화하려면 먼저 HTML 파일을 만들어야 한다.

```bash
$ cd docs && make html # make sure you are inside docs folder first
```

`_build/html` 내 `index.html` 파일이 생성되며, 브라우저에서 열면 다음과 같은 결과를 출력한다.

![](./images/1_ke-MBjZHjwWTdQewVuZDxg.webp)

프로젝트가 성장함에 따라 새로운 모듈을 추가하면 자동화된 문서화 프로세스에 해당 모듈이 포함되도록 해야 한다. 이 작업은 `docs/modules.rst` toctree에 새 모듈을 추가하여 수행할 수 있다.

```
advance_docs
============

.. toctree::
   :maxdepth: 4

   car
   # add new modules here as your project grows
```

## 마치며
테스트, 적절한 문서화 등을 추가하여 성장과 확장성 측면에서 프로젝트를 탄력적으로 만드는 중요한 측면을 다루었다.

이것은 1부에 불과하다. 2부에서는 더 중요한 주제에 대해 논의하고 여기서 다룬 주제에 대해 더 자세히 살펴볼 계획이다.

프로젝트는 생명체와 같아서 태어나고(시작), 성장하고(규모를 키우고), 번성(또는 우리가 원치 않는 죽음을 맞이)한다는 사실을 기억하자.

1부는 여기까지이다. 
