# 고급 Python 프로젝트 구조화 <sup>[1](#footnote_1)</sup>

이전 포스팅인 [Python 프로젝트 구조화](https://pythonnecosystems.gitlab.io/structuring-python-project/)에서는 모듈, 스크립트, 패키지와 같은 기본 개념에 대해 설명했다.

이제 프로젝트 구조화의 기본 사항을 살펴보았으니 이제 Python 프로젝트의 수명을 결정하는 요소들에 대해 더 자세히 알아보겠다.

코드를 어떻게 만들고, 정리하고, 문서화하느냐에 따라 수명이 길어지기도 하고, 순식간에 사라지기도 하는데, 우리는 순식간에 사라지는 것을 원하지 않는다.

예를 들어 우리가 회사를 떠난 후에도 프로젝트의 수명이 길고 건강하게 유지되기를 바란다. 이것이 바로 우리가 만들어가는 유산인 것이다. 우리의 기억과 흔적이지요.

그래서 장수 프로젝트를 만드는 데 도움이 되도록 프로젝트를 구조화하는 방법에 대한 고급 주제를 다루기 위해 이 포스팅을 쓰기로 하였다.

이 포스팅에서는 [이전 글](https://pythonnecosystems.gitlab.io/structuring-python-project/)에서 설명한 기본 개념을 바탕으로 Python 프로젝트 구조화와 관련된 고급 주제를 살펴본다.

이 포스팅에서 다음 내용을 다룰 것이다.

- **PyPA**라고도 하는 Python 패키지 Authority
- 프로젝트 종속성 관리
- Pytest를 사용한 프로젝트 테스트
- 자동화된 프로젝트 문서화

중요한 내용으로 들어가 보자.

<a name="footnote_1">1</a>: 이 페이지는 [Advanced Project Structuring — Python](https://python.plainenglish.io/advanced-project-structuring-python-part-i-d74f342e58f0)를 편역하였다.
